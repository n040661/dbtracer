package com.github.ghsea.dbtracer.sample.service;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.ghsea.dbtracer.interceptor.DbTraceEntry;
import com.github.ghsea.dbtracer.sample.module.TestModule;
import com.github.ghsea.dbtracer.sample.repository.TestModuleDao;

@Service
public class TestModuleService {

	@Resource
	private TestModuleDao testModuleDao;

	@DbTraceEntry(bizName = "测试update2Times")
	@Transactional(rollbackFor = Exception.class)
	public void update2Times() {
		TestModule module = new TestModule();
		module.setId(1L);
		module.setAddress("wuhan23");
		module.setCreateTime(new Date());
		testModuleDao.updateByPk(module);

		module.setAddress("wuhan25");
		module.setCreateTime(new Date());
		testModuleDao.updateByPk(module);

//		throw new RuntimeException("I'm just a test to check DbTracerContext.fail()");
	}
}
