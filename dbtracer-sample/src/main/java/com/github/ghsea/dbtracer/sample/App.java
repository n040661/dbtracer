package com.github.ghsea.dbtracer.sample;

import com.github.ghsea.dbtracer.Bootstrap;
import com.github.ghsea.dbtracer.interceptor.DbTracerContext;
import com.github.ghsea.dbtracer.sample.service.SpringUtil;
import com.github.ghsea.dbtracer.sample.service.TestModuleService;

public class App {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		Bootstrap.setUp("dbtrace.properties");
		// 到dao操作的这个过程不能切换线程，否则值无法传递
		DbTracerContext.setOperator("ghsea");

		TestModuleService service = SpringUtil.getBean(TestModuleService.class);
		service.update2Times();
	}

}
