package com.github.ghsea.dbtracer.interceptor;

import java.util.List;

import com.google.common.collect.Lists;

/**
 * Flag:当前线程是否需要执行dbtrace逻辑
 * 
 * @author GuHai 2017-8-11下午10:49:07
 */
public class DbTracerContext {
	private static ThreadLocal<BizValue> bizValue = new ThreadLocal<BizValue>() {
		protected BizValue initialValue() {
			return new BizValue();
		}
	};

	static void setNeedTrace(boolean need) {
		bizValue.get().setNeedTrace(need);
	}

	static void setBizName(String bizName) {
		bizValue.get().setBizName(bizName);
	}

	public static void clean() {
		bizValue.remove();
	}

	public static void setOperator(String operator) {
		bizValue.get().setOperator(operator);
	}

	public static BizValue getBizValue() {
		return bizValue.get();
	}

	public static class BizValue {
		private boolean needTrace = false;
		private String bizName;
		private String operator;

		/**
		 * 用于事务回滚的id
		 */
		private List<String> transactionIds;

		BizValue() {
			transactionIds = Lists.newArrayList();
		}

		public boolean isNeedTrace() {
			return needTrace;
		}

		void setNeedTrace(boolean needTrace) {
			this.needTrace = needTrace;
		}

		public String getBizName() {
			return bizName;
		}

		void setBizName(String bizName) {
			this.bizName = bizName;
		}

		public String getOperator() {
			return operator;
		}

		public void setOperator(String operator) {
			this.operator = operator;
		}

		public List<String> getTransactionIds() {
			return transactionIds;
		}

		void setTransactionIds(List<String> transactionIds) {
			this.transactionIds = transactionIds;
		}

	}

}
