package com.github.ghsea.dbtracer.util;

import java.util.List;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

public class SQLUtils {

	private SQLUtils() {

	}

	public static String join(List<String> list) {
		List<String> listByQuotes = Lists.transform(list, new Function<String, String>() {
			public String apply(String input) {
				StringBuffer sb = new StringBuffer(input.length() + 2);
				return sb.append("'").append(input).append("'").toString();
			}
		});

		return Joiner.on(",").join(listByQuotes);
	}
}
